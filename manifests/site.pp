node default{
  
    package { 'php':
     ensure => present,
     before => File['/etc/php.ini'],
    }

    file {'/etc/php.ini':
     ensure => file,
    }

    package { 'httpd':
     ensure => installed,   
 }

   service {'httpd':
     ensure => running,
     enable => true,
     }

   package {'mysql-server':
     ensure => present,
     }

   service {'mysqld':
     ensure => running,
     enable => true,
     }









 

}
